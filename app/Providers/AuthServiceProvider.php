<?php

namespace App\Providers;

use App\Domains\Users\Model\User;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            $auth = $request->header('Authorization');
            if ($auth) {

                if (!$auth) {
                    // Unauthorized response if token not there
                    return response()->json([
                        'error' => 'Token not provided.'
                    ], 401)->send();
                }

                try {
                    $credentials = JWT::decode($auth, env('JWT_SECRET'), ['HS256']);

                    $user = User::where("id", $credentials->sub)
                        ->select('id', 'email', 'nome', 'id as user_id')
                        ->first();
                    // Now let's put the user in the request class so that you can grab it from there
                    if (!empty($user)) {
                        $request->request->add(['userid' => $user->id]);
                    }

                    return $user;

                } catch (ExpiredException $e) {
                    return response()->json([
                        'error' => 'Provided token is expired.'
                    ], 400)->send();

                } catch (\Exception $e) {
                    return response()->json([
                        'error' => $e->getMessage()
                    ], 400)->send();
                }
            }
        });
    }
}
