<?php

namespace App\Domains\Users\Service;

use App\Domains\Users\Model\User;

class ServiceUser
{
    private $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function findByEmail(string $email)
    {
        return $this->model
            ->where('email', $email)
            ->first();
    }
}
