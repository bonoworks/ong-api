<?php

namespace App\Domains\Login\Controllers;

use App\Domains\Users\Service\ServiceUser;
use App\Domains\Users\Model\User;
use App\Http\Controllers\Controller;
use Firebase\JWT\JWT;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    private $service;

    public function __construct(ServiceUser $user)
    {
        $this->service = $user;
    }

    public function authenticate(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            $user = $this->service->findByEmail($request->input('email'));

            if (is_null($user)) {
                throw new \Exception("Usuário/Senha não encontrado");
            }

            if ((new BcryptHasher())->check($request->input('password'), $user->senha)) {

                return response()->json(['status' => 'success', 'api_key' => $this->jwt($user)]);

            } else {
                return response()->json(['status' => 'fail'], 401);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }
    }

    /**
     * @param User $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

}
