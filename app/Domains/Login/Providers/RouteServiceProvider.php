<?php

namespace App\Domains\Login\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $namespace = '\App\Domains\Login\Controllers';

    public function register()
    {
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes()
    {
        $this->app->router->group(['prefix'=>'login', 'namespace' => $this->namespace], function ($app) {
            require __DIR__.'/../Routes/api.php';
        });
    }
}
