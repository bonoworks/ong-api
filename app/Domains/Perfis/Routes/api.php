<?php

$app->get('/{tipo}', ['uses' => 'PerfilController@index']);
$app->get('/{tipo}/{id}', ['uses' => 'PerfilController@show']);
$app->post('/{tipo}', ['uses' => 'PerfilController@create']);
$app->put('/{tipo}/{id}', ['uses' => 'PerfilController@update']);
$app->delete('/{tipo}/{id}', ['uses' => 'PerfilController@destroy']);
