<?php

namespace App\Domains\Perfis\Controllers;

use App\Domains\Perfis\FactoryPerfil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PerfilController extends Controller
{
    private $factory;

    /**
     * PerfilController constructor.
     * @param FactoryPerfil $factoryPerfil
     */
    public function __construct(FactoryPerfil $factoryPerfil)
    {
        $this->middleware('auth');
        $this->factory = $factoryPerfil;
    }

    /**
     * @param $tipo
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($tipo)
    {
        $perfis = $this->factory::build($tipo)
            ->get();

        return response()->json(['data' => $perfis], Response::HTTP_OK);
    }

    /**
     * @param $tipo
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($tipo, $id)
    {
        $perfil = $this->factory::build($tipo)
            ->show($id);

        return response()->json(['data' => $perfil], Response::HTTP_OK);
    }

    /**
     * @param $tipo
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($tipo, Request $request)
    {
        try {
            $perfil = $this->factory::build($tipo)
                ->create($request->all());

            return response()->json(['data' => $perfil], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $tipo
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($tipo, $id, Request $request)
    {
        try {
            $perfil = $this->factory::build($tipo)
                ->update($id, $request->all());

            return response()->json(['data' => $perfil], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
    }

    /**
     * @param $tipo
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($tipo, $id)
    {
        try {
            $perfil = $this->factory::build($tipo)
                ->delete($id);

            return response()->json(['data' => $perfil], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
