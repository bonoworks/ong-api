<?php

namespace App\Domains\Perfis;

class FactoryPerfil
{
    protected static $_instance;

    public static function build($class, ...$params)
    {
        $class = self::prepareClass($class);

        if (null === static::$_instance) {

            static::$_instance = $class->newInstanceArgs($params);
        }

        return static::$_instance = $class->newInstanceArgs($params);
    }

    protected static function prepareClass($class)
    {
        $class_namespace = self::getNameSpace($class);

        if (!class_exists($class_namespace)) {
            $class_namespace = self::getNameSpace('DefaultStatus');
        }
        return new \ReflectionClass($class_namespace);
    }

    protected static function getNameSpace($class)
    {
        return "App\\Domains\\Perfis\\Strategy\\" . $class;
    }
}
