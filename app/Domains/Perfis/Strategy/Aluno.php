<?php

namespace App\Domains\Perfis\Strategy;

class Aluno implements IPerfil
{
    public function get()
    {
        return "aluno";
    }

    public function show(int $id)
    {
        return "aluno";
    }

    public function create(array $request)
    {
        return "aluno";
    }

    public function update(array $request)
    {
        return "aluno";
    }

    public function delete(int $id)
    {
        return "aluno";
    }

}
