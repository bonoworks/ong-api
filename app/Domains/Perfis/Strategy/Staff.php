<?php

namespace App\Domains\Perfis\Strategy;

class Staff implements IPerfil
{
    public function get()
    {
        return "Staff";
    }

    public function show(int $id)
    {
        return "Staff";
    }

    public function create(array $request)
    {
        return "Staff";
    }

    public function update(array $request)
    {
        return "Staff";
    }

    public function delete(int $id)
    {
        return "Staff";
    }
}
