<?php

namespace App\Domains\Perfis\Strategy;

class Patrocinador implements IPerfil
{
    public function get()
    {
        return "Patrocinador";
    }

    public function show(int $id)
    {
        return "Patrocinador";
    }

    public function create(array $request)
    {
        return "Patrocinador";
    }

    public function update(array $request)
    {
        return "Patrocinador";
    }

    public function delete(int $id)
    {
        return "Patrocinador";
    }
}
