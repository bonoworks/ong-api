<?php

namespace App\Domains\Perfis\Strategy;

interface IPerfil
{
    public function get();
    public function show(int $id);
    public function create(array $request);
    public function update(array $request);
    public function delete(int $id);
}
