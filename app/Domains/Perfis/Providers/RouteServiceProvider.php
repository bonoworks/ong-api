<?php

namespace App\Domains\Perfis\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $namespace = '\App\Domains\Perfis\Controllers';

    public function register()
    {
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes()
    {
        $this->app->router->group(['prefix'=>'perfis', 'namespace' => $this->namespace], function ($app) {
            require __DIR__.'/../Routes/api.php';
        });
    }
}
